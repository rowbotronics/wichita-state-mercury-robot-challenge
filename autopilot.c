#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
//#include <unistd.h> Sleep function
#include "autopilot.h"
#define MARGIN_OF_ERROR 1
#define MAX_FRONT_DIST 30
#define MAX_SIDE_DIST 10
#define MIN_FRONT_DIST 3
#define MIN_SIDE_DIST 3
#define ROBOT_WIDTH 30.48
#define DEFAULT_SPEED /*GIVE SOME DEFAULT SPEED IN CM/S*/
#define PI 3.141592653589793238462643383

State state = stopped;
const float WHEEL_RADIUS = 1.75;
float rightWheelVelocity = DEFAULT_SPEED;
float leftWheelVelocity = DEFAULT_SPEED;

//Arrays of distances for sensors.

int UltraSensorDists[2] = {0,0};
int InfraSensorDists[4] = {0,0,0,0};


int main()
{	
	//Execute forever until robot stops (indicating error).
	
	do
	{
//		pingAll(); **HERE IS WHERE WE SHOULD IMPLEMENT CHRISTIAN'S CODE FOR POLLING THE SENSORS.**
		
		//If there is a wall in front of the robot and close enough to be pertinent, turn in the correct direction.
		//If there is no wall in front and the robot is turning, go straight.
		//If the robot is going straight and is too close to the side, turn slightly and straighten out again. 
		
		if((UltraSensorDists[front_l] < MAX_FRONT_DIST && UltraSensorDists[front_r] < MAX_FRONT_DIST) &&
		(UltraSensorDists[front_l] > MIN_FRONT_DIST || UltraSensorDists[front_l] > MIN_FRONT_DIST))
		{
			/*Determine whether there is a gradient in the wall and the direction of the gradient.
			 *Turn in the corresponding direction.
			 */
		
			if(UltraSensorDists[front_l] < UltraSensorDists[front_r])
			{
				//Only turn right if there is a forward gradient in the right wall or the wall does not exist going forward.
				
				if(InfraSensorDists[right_f] > MAX_FRONT_DIST || InfraSensorDists[right_f] >= InfraSensorDists[right_b])
				{
					printf("Turning right...\n");
					turnRight((float)(UltraSensorDists[front_r]-UltraSensorDists[front_l]));
					state = turning;
				}
				else
				{
					printf("Robot trapped. Stopping...\n");
					stop();
					state = stopped;
				}
			}
			else if(UltraSensorDists[front_l] > UltraSensorDists[front_r])
			{
				//Only turn left if there is a forward gradient in the left wall or the wall does not exist going forward.
				
				if(InfraSensorDists[left_f] > MAX_SIDE_DIST || InfraSensorDists[left_f] > InfraSensorDists[left_b])
				{
					printf("Turning left...\n");
					turnLeft(UltraSensorDists[front_l]-UltraSensorDists[front_r]);
					state = turning;
				}
				else
				{
					printf("Robot trapped. Stopping...\n");
					stop();
					state = stopped;
				}
			}
			else
			{
				/*If there is no wall to the left, turn left. If no wall to the right, turn right (unless there is also no wall to the left). 
				 *If walls to both the right and left, stop.
				 */
				 
				 if(InfraSensorDists[left_f] > MAX_SIDE_DIST)
				 {
					printf("Turning left...\n");
					turnLeft(0);
					state = turning;
				 }
				 else if(InfraSensorDists[right_f] > MAX_SIDE_DIST)
				 {
					printf("Turning right...\n");
					turnRight(0);
					state = turning;
				 }
				 else
				 {
					printf("Robot trapped. Stopping...\n");
					stop();
					state = stopped;
				 }
			}
		}
		else if(state == turning)
		{
			printf("Going straight...\n");
			goStraight();
			state = going_straight;
		}
		else if(InfraSensorDists[left_f] < MIN_SIDE_DIST)
		{
			printf("Too close! Moving to the right...\n");
			scootRight();
		}
		else if(InfraSensorDists[right_f] < MIN_SIDE_DIST)
		{
			printf("Too close! Moving to the left...\n");
			scootLeft();
		}
		
		//Sleep 50 ms before continuing.
		
		Sleep(50);
	}
	while(state != stopped);
	return 0;
}

//Ping all sensors. **NEEDS TO BE IMPLEMENTED**

void pingAll()
{
/*	int i = 0;
	int j = 0;
	for(; i < infras.length; i++)	InfraSensorDists[i] = analogRead(InfraredSensorPins[i]);
	for(; j < ultras.length; j++)	UltraSensorDists[j] = getDistance(initPinUltraSensors[j], echoPinUltraSensors[j]);*/
 
}

void turnRight(float gradient)
{
	float turnAngle = PI/2 - atanf(gradient/ROBOT_WIDTH);
	/*float angularVelocityDelta = (ROBOT_WIDTH * turnAngle * DEFAULT_SPEED) 
		/ (min(UltraSensorDists[front_r], UltraSensorDists[front_l]) * WHEEL_RADIUS);*/
	rightWheelVelocity = (DEFAULT_SPEED * (min(UltraSensorDists[front_r], UltraSensorDists[front_l]) * WHEEL_RADIUS))
		/ WHEEL_RADIUS * (ROBOT_WIDTH * turnAngle * DEFAULT_SPEED + (min(UltraSensorDists[front_r], UltraSensorDists[front_l]) * WHEEL_RADIUS));
	leftWheelVelocity = DEFAULT_SPEED / WHEEL_RADIUS;
	state = turning;
}

//Send sensor signal to turn left based on a certain gradient.

void turnLeft(float gradient)
{
	float turnAngle = PI/2 - atanf(gradient/ROBOT_WIDTH);
	/*float angularVelocityDelta = (ROBOT_WIDTH * turnAngle * DEFAULT_SPEED) 
		/ (min(UltraSensorDists[front_r], UltraSensorDists[front_l]) * WHEEL_RADIUS);*/
	leftWheelVelocity = (DEFAULT_SPEED * (min(UltraSensorDists[front_r], UltraSensorDists[front_l]) * WHEEL_RADIUS))
		/ WHEEL_RADIUS * (ROBOT_WIDTH * turnAngle * DEFAULT_SPEED + (min(UltraSensorDists[front_r], UltraSensorDists[front_l]) * WHEEL_RADIUS));
	rightWheelVelocity = DEFAULT_SPEED / WHEEL_RADIUS;
	state = turning;
}

//Send sensor signal to stop.

void stop()
{
	leftWheelVelocity = 0;
	rightWheelVelocity = 0;
	state = stopped;
}

//Straighten out the robot.

void goStraight()
{
	rightWheelVelocity = DEFAULT_SPEED / WHEEL_RADIUS;
	leftWheelVelocity = DEFAULT_SPEED / WHEEL_RADIUS;
	state = going_straight;
}

//Move to the right for a short distance, then straighten out.

void scootRight()
{
	turnRight(ROBOT_WIDTH / 2);
	Sleep(5);
	turnLeft(ROBOT_WIDTH / 2);
	Sleep(5);
	goStraight();
}

//Move to the left for a short distance, then straighten out.

void scootLeft()
{
	turnLeft(ROBOT_WIDTH / 2);
	Sleep(5);
	turnRight(ROBOT_WIDTH / 2);
	Sleep(5);
	goStraight();
}