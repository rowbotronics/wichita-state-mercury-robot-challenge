typedef enum STATE {turning, going_straight, stopped} State;
enum FRONT_SENSOR_POS {front_l, front_r};
enum SIDE_SENSOR_POS {left_b, left_f, right_f, right_b};

void pingAll();
void turnRight(float gradient);
void turnLeft(float gradient);
void stop();
void goStraight();
void scootRight();
void scootLeft();
